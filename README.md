# Front-end Developer Exercise #
1. Clone this repository
1. Build a single webpage based on the attached .psd
1. Create a zip with resource files and share with us

### Success Criteria ###
* Fully Responsive
* Minimally compatible with:
    * Internet Explorer 10
    * Chrome 30
    * Safari 6.1
    * Firefox 30
* Markup, styles, scripts, and project structure should be well organized and thoughtful
* Project should match comps
* Project should run in any required browser

### Notes ###
* The provided .psd does not include a mobile view, or branded images -- this means you get some freedom in what those things look like; spice it up, or keep them simple
* Show us you know your way around HTML, CSS, and JS
* Leverage any library or build tool you like, but have a good reason for it

### Resources ###
* We have provided several deliverables to help with construction of the page
* All of the copy, images, font links, icons, and the .psd can be found in the ./resources/ folder
* Feel free to slice out any aspects of the comp if you need to